# Flurry

## Integration Steps

1) **"Install"** or **"Upload"** FG Flurry plugin from the FunGames Integration Manager or download it from <a href="https://drive.google.com/uc?export=download&id=1RnhqSoXiK50xpUoP3qQbb1GfA4NDbGVX" target="_blank">here</a>.

2) Follow the instructions in the **"Install External Plugin"** section to import Flurry SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG Flurry module from the Integration Manager window, you will find the last compatible version of Flurry SDK in the _Assets > FunGames_Externals > Analytics_ folder. Double click on the .unitypackage file to install it.

If you wish to install a newest version of their SDK, you can also download it from the <a href="https://assetstore.unity.com/packages/tools/integration/flurry-sdk-plugin-214411" target="_blank">Unity Asset Store</a> or <a href="https://github.com/flurry/unity-flurry-sdk" target="_blank">GitHub</a>.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**

## Account and Settings

To finish your Flurry setup you will need to create an app on Flurry dashboard, and add the corresponding keys to your FG Flurry Settings (_Assets > Resources > FunGames > FGFlurrySettings_).